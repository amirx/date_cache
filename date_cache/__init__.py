from boltons.funcutils import wraps

from date_cache.IntervalDiskCache import IntervalDiskCache

base_cache_directory = ".interval_cache"


def interval_disk_cache(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        return wrapper.interval_cache.get_data(*args, **kwargs)

    wrapper.interval_cache = IntervalDiskCache(
        func,
        base_cache_directory,
        start_param="start_time",
        end_param="end_time",
        time_column="creation_time",
        primary_key_column="id",
    )

    return wrapper





