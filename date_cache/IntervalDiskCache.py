import inspect
import os
import datetime
import pandas as pd


def get_function_signature(func):
    return inspect.signature(func)


class WrongTimeParamTypeException(object):
    pass


class FunctionMissingParameterException(Exception):
    pass


def get_time(time_object):
    if type(time_object) is datetime.datetime:
        return time_object
    raise WrongTimeParamTypeException()


def get_param_value(func, param_name, *args, **kwargs):
    param_loc_index = get_param_location(func, param_name)
    if param_loc_index < len(args):
        return args[param_loc_index]
    if param_name in kwargs:
        return kwargs[param_name]
    raise FunctionMissingParameterException()


def get_param_location(func, param_name):
    signature = inspect.signature(func)
    param_loc_index = list(signature.parameters.keys()).index(param_name)
    return param_loc_index


def next_missing_interval(cur_start_time, cur_end_time, existing_intervals):
    return cur_start_time, cur_end_time


class IntervalParams:
    def __init__(self, start_time, end_time, data):
        self.start_time = start_time
        self.end_time = end_time
        self.data = data


def replace_function_params(func, param_name, param_value, args, kwargs):
    param_location = get_param_location(func, param_name)
    if param_location < len(args):
        args[param_location] = param_value
    elif param_name in kwargs:
        kwargs[param_name] = param_value
    else:
        raise FunctionMissingParameterException()


class IntervalDiskCache:
    def __init__(
            self,
            func,
            base_cache_directory,
            start_param,
            end_param,
            time_column,
            primary_key_column,
    ):
        cache_signature = f"{get_function_signature(func)}_{start_param}_{end_param}_{time_column}_{primary_key_column}"
        self.func = func
        self.cache_directory = os.path.join(base_cache_directory, cache_signature)
        self.start_param = start_param
        self.end_param = end_param
        self.time_column = time_column
        self.primary_key_column = primary_key_column
        self.existing_intervals = []

        os.makedirs(self.cache_directory, exist_ok=True)

    def get_data(self, *args, **kwargs):
        start_time, end_time = self.get_interval(*args, **kwargs)
        data = pd.DataFrame()
        while start_time < end_time:
            covering_interval = self.find_covering_interval(start_time)
            if covering_interval is not None:
                covering_df = covering_interval.data
                data = data.append(covering_df[
                                (covering_df[self.time_column] >= start_time) &
                                (covering_df[self.time_column] < end_time)
                ])
                start_time = covering_interval.end_time
            else:
                next_start_interval_start_time = self.get_next_start_time(start_time, end_time)
                data = data.append(self.get_data_for_interval(start_time, next_start_interval_start_time, args, kwargs))
                start_time = next_start_interval_start_time
        return data

    def get_interval(self, *args, **kwargs):
        start_time = get_time(get_param_value(self.func, self.start_param, *args, **kwargs))
        end_time = get_time(get_param_value(self.func, self.end_param, *args, **kwargs))
        return start_time, end_time

    def find_covering_interval(self, start_time):
        for interval in self.existing_intervals:
            if interval.start_time <= start_time < interval.end_time:
                return interval

        return None

    def get_next_start_time(self, start_time, end_time):
        closest_start_time = end_time
        for interval in self.existing_intervals:
            if start_time < interval.start_time < closest_start_time:
                closest_start_time = interval.start_time

        return closest_start_time

    def get_data_for_interval(self, start_time, end_time, args, kwargs):
        new_args, new_kwargs = self.replace_params(start_time, end_time, args, kwargs)
        data = self.func(*new_args, **new_kwargs)
        self.cache_data(data, end_time, start_time)
        return data

    def cache_data(self, data, end_time, start_time):
        self.existing_intervals.append(IntervalParams(start_time, end_time, data))

    def replace_params(self, start_time, end_time, args, kwargs):
        args = list(args)
        kwargs = kwargs.copy()
        replace_function_params(self.func, self.start_param, start_time, args, kwargs)
        replace_function_params(self.func, self.end_param, end_time, args, kwargs)
        return tuple(args), kwargs

