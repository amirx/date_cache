import pandas as pd
import date_cache as pac
from dateutil import parser


def tomorrow_df(start_time, end_time):
    print(f"querying from {start_time} to {end_time}")
    return pd.DataFrame(pd.date_range(start_time, end_time), columns=["creation_time"])

cached_tomorrow_df = pac.interval_disk_cache(tomorrow_df)

cached_tomorrow_df(parser.parse("2018-01-01"), parser.parse("2018-01-05"))
cached_tomorrow_df(parser.parse("2018-01-03"), parser.parse("2018-01-08"))

