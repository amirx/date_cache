from setuptools import setup, find_packages
 
 
setup(
      name='date_cache',
      version='0.3',
      url='https://gitlab.com/amirx/date_cache',
      license='MIT',
      author='Amir Hossein Bakhtiary Davijani',
      author_email='amir.h.bakhtiary@gmail.com',
      description='Manage configuration files',
      packages=find_packages(exclude=['tests']),
      install_requires=[
            'boltons',
            'pandas'
      ],
      long_description=open('README.md').read(),
      zip_safe=False,
      setup_requires=['nose>=1.0'],
      test_suite='nose.collector'
)
